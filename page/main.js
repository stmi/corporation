//Java Script code for Corporation Char Mgr

Vue.component('summary-field', {
  template: `<input id="summary-input" :value=this.textValue v-on:input="updateValue($event.target.value)" placeholder="Input Text" required="required">`,
  props: {
    fieldName: {
      type: String,
      required: true,
      validator: function (value) {
        // The value must match one of these strings
        return ['name', 'corporation', 'rank', 'profession', 'division'].indexOf(value) !== -1
      }
    }
  },
  data: function() {
    return {
      textValue: ""
    }
  },
  methods: {
    updateValue(eventValue) {
      this.textValue = eventValue
      this.$emit('update-summary-field', this.fieldName, this.textValue)
    }
  }
})

Vue.component('primary-stat', {
  template: 
  `<div>
    <button type="button" v-on:click=decrValue>-</button>
    <input id="stat-input" :value=this.statValue min=1 max=10>
    <button type="button" v-on:click=incrValue>+</button>
  </div>`,
  props: {
    statName: {
      type: String,
      required: true,
      validator: function (value) {
        // The value must match one of these strings
        return ['strength', 'endurance', 'agility', 'reflexes', 'perception', 'intelligence', 'presence'].indexOf(value) !== -1
      }
    },
    initialValue: {
      type: Number,
      default: 7
    }
  },
  data: function(){
      return {
        statValue : this.initialValue
      }
  },
  methods: {
    decrValue() {
      this.statValue -= 1
      this.$emit('update-primary-stat', this.statName, this.statValue)
    },
    incrValue() {
      this.statValue += 1
      this.$emit('update-primary-stat', this.statName, this.statValue)
    }
  }
})

var app = new Vue({
  el: '#app',
  data: {
    summary: {
      name: 'None',
      corporation: 'None',
      rank: 1,
      profession: 'None',
      division: 'None',
    },
    stats: {
      strength: 7,
      endurance: 7,
      agility: 7,
      reflexes: 7,
      perception: 7,
      intelligence: 7,
      presence: 7
    },
    endpoint: 'https://corporation.cfapps.io/'
  },
  methods: {
    updateSummaryField(fieldName, textValue) {
      this.summary[fieldName] = textValue
    },
    updatePrimaryStat(statName, statValue) {
      this.stats[statName] = statValue
    },
    createCharacterCall() {
      let character = {
        summary: this.summary,
        stats: this.stats
      }
      axios.post(
        this.endpoint + 'createCharacter',
        character)
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    printCharacterCall() {
        let character = {
          summary: this.summary,
          stats: this.stats
        }
        axios.post(this.endpoint + 'printCharacter', character, {responseType: 'blob'})
          .then(response => {
            const content = response.headers['content-type'];
            download(response.data, 'character.pdf', content)
            })
          .catch(function (error) {
            console.log(error);
          });
        }
    }
})