package eu.wiktordolecki.corporation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Summary {

    String name;
    String corporation;
    String rank;
    String profession;
    String division;
}
