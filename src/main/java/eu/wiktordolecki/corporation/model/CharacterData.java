package eu.wiktordolecki.corporation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CharacterData {

    private Summary summary;
    private PrimaryStats stats;
}
