package eu.wiktordolecki.corporation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PrimaryStats {
    int strength;
    int endurance;
    int agility;
    int reflexes;
    int perception;
    int intelligence;
    int presence;
}
