package eu.wiktordolecki.corporation.pdf;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;
import eu.wiktordolecki.corporation.model.CharacterData;

import eu.wiktordolecki.corporation.model.PrimaryStats;
import eu.wiktordolecki.corporation.model.Summary;
import java.io.ByteArrayOutputStream;

public class CharacterPdfCreator {

     public byte[] printCharacterToByteArray(CharacterData character) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        final PdfWriter pdfWriter = new PdfWriter(outputStream);
        final PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        try (final Document document = new Document(pdfDocument)) {
            document.add(new Paragraph("Corporation Character"));
            document.add(new Paragraph("OBI Stark Demo"));
            writeSummaryTable(character, document);
            writePrimaryStatsTable(character, document);
        }

        return outputStream.toByteArray();
    }

    private void writePrimaryStatsTable(CharacterData character, Document document) {
        document.add(new Paragraph("Primary Stats"));

        Table statsTable = new Table(UnitValue.createPercentArray(2)).useAllAvailableWidth();
        PrimaryStats primaryStats = character.getStats();

        statsTable.addCell("Strength");
        statsTable.addCell(Integer.toString(primaryStats.getStrength()));
        statsTable.addCell("Endurance");
        statsTable.addCell(Integer.toString(primaryStats.getEndurance()));
        statsTable.addCell("Agility");
        statsTable.addCell(Integer.toString(primaryStats.getAgility()));
        statsTable.addCell("Reflexes");
        statsTable.addCell(Integer.toString(primaryStats.getReflexes()));
        statsTable.addCell("Perception");
        statsTable.addCell(Integer.toString(primaryStats.getPerception()));
        statsTable.addCell("Intelligence");
        statsTable.addCell(Integer.toString(primaryStats.getIntelligence()));
        statsTable.addCell("Presence");
        statsTable.addCell(Integer.toString(primaryStats.getPresence()));

        document.add(statsTable);
    }

    private void writeSummaryTable(CharacterData character, Document document) {
        Table summaryTable = new Table(UnitValue.createPercentArray(2)).useAllAvailableWidth();

        document.add(new Paragraph("Summary"));

        Summary summary = character.getSummary();
        summaryTable.addCell("Name");
        summaryTable.addCell(summary.getName());
        summaryTable.addCell("Corporation");
        summaryTable.addCell(summary.getCorporation());
        summaryTable.addCell("Rank");
        summaryTable.addCell(summary.getRank());
        summaryTable.addCell("Profession");
        summaryTable.addCell(summary.getProfession());
        summaryTable.addCell("Division");
        summaryTable.addCell(summary.getDivision());

        document.add(summaryTable);
    }
}
